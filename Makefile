#############  program name
	TARGET		= JN5121FreeRTOS

	TOOL		= ba-elf
#	TOOLPOTFIX	= .exe

#	_NUL 		= /dev/null
	_NUL		= nul

	CHIP		= JN5121
	JENNIC_PCB  = DEVKIT1
	OPTIMIZE	= -O3
	DEBUG		= 1
	USERELISEOS = 0

###########################################################
#  Toolnames
###########################################################

	BASE		= .
	UTILSDIR	= $(BASE)/../../utils/bin
	CC_			= $(TOLLPATH)$(TOOL)-gcc$(TOOLPOTFIX)
#	CC_			= $(TOLLPATH)$(TOOL)-g++$(TOOLPOTFIX)
	CXX			= $(TOLLPATH)$(TOOL)-g++$(TOOLPOTFIX)
	LD			= $(TOLLPATH)$(TOOL)-ld$(TOOLPOTFIX)
	AS			= $(CC) -x assembler-with-cpp
	OBJCOPY		= $(TOLLPATH)$(TOOL)-objcopy$(TOOLPOTFIX)
	OBJDUMP		= $(TOLLPATH)$(TOOL)-objdump$(TOOLPOTFIX)
	SIZE		= $(TOLLPATH)$(TOOL)-size$(TOOLPOTFIX) #-A
	READELF		= $(TOLLPATH)$(TOOL)-readelf$(TOOLPOTFIX) -a
	AR			= $(TOLLPATH)$(TOOL)-ar$(TOOLPOTFIX)
	RAR			= rar
	TAR			= tar
	RM			= rm -f
	CP			= cp
	MD			= mkdir
	BINPATCH	= $(UTILSDIR)/checksumm
	FLASHER		= $(UTILSDIR)/../flashprogrammer/FlashCLI
	
#  dirs
	SRCDIR					 = $(BASE)/src
	INCDIR					 = $(SRCDIR)/inc
	APPDIR					 = $(SRCDIR)/App
	OBJDIR					 = $(BASE)/obj
	EXEDIR					 = $(BASE)/exe
	LSTDIR					 = $(BASE)/lst
	PRJDIR					 = $(BASE)/prj
	BAKDIR					 = $(BASE)/bak
	
	OS_BASE_DIR				 = $(SRCDIR)/FreeRTOS
	
###########################################################
# SDK
###########################################################
	JENNIC_SDK_BASE_DIR      = $(BASE)/../../lib/JennicSdk
	
	JENNIC_BUILD_CFG_DIR     = $(JENNIC_SDK_BASE_DIR)/Common/Build
	JENNIC_COMMON_DIR        = $(JENNIC_SDK_BASE_DIR)/Common/Include
	JENNIC_CHIP_INCLUDE_DIR  = $(JENNIC_SDK_BASE_DIR)/Chip/$(CHIP)/Include
	JENNIC_CHIP_LIBRARY_DIR  = $(JENNIC_SDK_BASE_DIR)/Chip/$(CHIP)/Library
	JENNIC_CHIP_LINKER_DIR   = $(JENNIC_SDK_BASE_DIR)/Chip/$(CHIP)/Build
ifeq ($(JENNIC_PCB), DEVKIT1)
	JENNIC_BOARD_INCLUDE_DIR = $(JENNIC_SDK_BASE_DIR)/Platform/DK1/Include
	JENNIC_BOARD_LIBRARY_DIR = $(JENNIC_SDK_BASE_DIR)/Platform/DK1/Library
else
	JENNIC_BOARD_INCLUDE_DIR = $(JENNIC_SDK_BASE_DIR)/Platform/DK2/Include
	JENNIC_BOARD_LIBRARY_DIR = $(JENNIC_SDK_BASE_DIR)/Platform/DK2/Library
endif
	JENNIC_COMON_LIBRARY_DIR = $(JENNIC_SDK_BASE_DIR)/Common/Library

	JENNIC_CHIP 			 = $(CHIP)
	BASE_DIR 				 = $(JENNIC_SDK_BASE_DIR)
export JENNIC_CHIP
export JENNIC_PCB
export BASE_DIR
	CFLAGS 		  =

include $(JENNIC_BUILD_CFG_DIR)/config.mk

ifeq ($(DEBUG), 1)
	CFLAGS       := $(subst -Os,,$(CFLAGS))
	CFLAGS		 += -g -O0 -DGDB
	TYPE	 	  = debug
else
	TYPE	 	  = relise
ifneq ($(OPTIMIZE), "")
	CFLAGS       := $(subst -Os,,$(CFLAGS))
	CFLAGS		 += $(OPTIMIZE)
endif
endif

	CC = $(CC_)
	
###########################################################
#	Files
###########################################################
	ELF		= $(EXEDIR)/$(TARGET).elf
	BIN		= $(EXEDIR)/$(TARGET).bin
	MAP		= $(LSTDIR)/$(TARGET).map
	LSS		= $(LSTDIR)/$(TARGET).lss
	ELFINFO = $(LSTDIR)/$(TARGET).elfinfo
	OK		= $(EXEDIR)/$(TARGET).ok

# linker script
#	LD_SCRIPT	= $(JENNIC_CHIP_LINKER_DIR)/AppBuild_$(CHIP).ld
	LD_SCRIPT	= $(PRJDIR)/AppBuild_$(CHIP).ld
	
# libfiles

	LIBS	 =
	
ifeq ($(USERELISEOS), 1)
	LIBS	+= $(OS_BASE_DIR)/Output/FreeRTOSCore_relise.a
else
	LIBS	+= $(OS_BASE_DIR)/Output/FreeRTOSCore_$(TYPE).a
endif

	LIBS	+= $(JENNIC_CHIP_LIBRARY_DIR)/ChipLib.a
	LIBS	+= $(JENNIC_BOARD_LIBRARY_DIR)/BoardLib_$(CHIP).a
	LIBS	+= $(JENNIC_COMON_LIBRARY_DIR)/libc.a
	LIBS	+= $(JENNIC_COMON_LIBRARY_DIR)/libgcc.a
	LIBS	+= $(JENNIC_COMON_LIBRARY_DIR)/libm.a

# files
	DIRS	:= $(SRCDIR) $(APPDIR)
	INCDIRS  = $(DIRS) $(INCDIR) $(JENNIC_COMMON_DIR) $(JENNIC_CHIP_INCLUDE_DIR) $(JENNIC_BOARD_INCLUDE_DIR) $(OS_BASE_DIR)/inc
	INCS	:= $(patsubst %, -I "%", $(INCDIRS))
	SRCS	:= $(wildcard $(addsuffix /*.cpp, $(DIRS))) $(wildcard $(addsuffix /*.c, $(DIRS))) $(wildcard $(addsuffix /*.S, $(DIRS)))
	OBJS	:= $(notdir $(SRCS) )
	OBJS	:= $(OBJS:.cpp=.o)
	OBJS	:= $(OBJS:.c=.o)
	OBJS	:= $(OBJS:.S=.o)
	OBJS	:= $(patsubst %, $(OBJDIR)/%, $(OBJS))

	TARS	 = $(BASE)/makefile
	TARS	+= $(shell ls $(SRCDIR)/*.c)
	TARS	+= $(shell ls $(SRCDIR)/*.s)
	TARS	+= $(shell ls $(SRCDIR)/*.cpp)
	TARS	+= $(shell ls $(INCDIR)/*.h)
	TARS	+= $(shell ls $(INCDIR)/*.hpp)
	TARS	+= $(shell ls $(PRJDIR)/*)
	TARS	+= $(shell ls $(BASE)/.settings/*)
	TARS	+= $(subst ./,$(OS_BASE_DIR)/,$(shell cat $(OS_TARS)))
	
	OS_TARS  = $(OS_BASE_DIR)/SRCList.lst
	
	DATE	 = $(shell date +%F_%H-%M-%S)

# 
	ttyDEV 		= /dev/ttyUSB0
	ttySPEED 	= 115200

###########################################################
#	 flags
###########################################################
# Общие флаги
	FLAGS		 = $(INCS)

# Флаги для as
	AFLAGS		 = $(FLAGS)
#	AFLAGS	   	+= -adhlns=$(addprefix $(LSTDIR)/, $(notdir $(addsuffix .lst, $(basename $<))))

# Флаги для gcc
	CFLAGS		+= $(FLAGS) 
	CFLAGS		+= -ffunction-sections -fdata-sections
#	CFLAGS		+= -Wa,-adhlns=$(addprefix $(LSTDIR)/, $(notdir $(addsuffix .lst, $(basename $<))))

# Флаги для g++
	CXXFLAGS	 = $(CFLAGS)

# Флаги для ld
	LD_FLAGS	 = -L$(JENNIC_CHIP_LINKER_DIR)
	LD_FLAGS	+= -Map="$(MAP)"
	LD_FLAGS	+= -T$(LD_SCRIPT)
	LD_FLAGS	+= -u_AppColdStart -u_AppWarmStart
	LD_FLAGS	+= --gc-sections
	LD_FLAGS	+= -nostartfiles
	
# Настройки прошивальщика
	FLASHDEVICE			= /dev/ttyS1
	FLASHCOMPORT		= 2
	FILENAME			= $(TARGET).bin

.SILENT :

.PHONY: all dirs clean flash rebuild archive cu

###########################################################
#	 Targets
###########################################################

all : dirs $(ELF) $(BIN) $(LSS) $(OK)


$(LSS): $(ELF) makefile
	@echo --- making asm-lst...
	$(OBJDUMP) -D $(ELF) > $(LSS)

$(OK): $(ELF)
	$(READELF) $(ELF) > $(ELFINFO)
	$(SIZE) $(ELF)
	@echo "Errors: none"

$(ELF):	$(OBJS) makefile
	@echo --- linking...
	$(LD) $(OBJS) $(LIBS) $(LD_FLAGS) -o $(ELF)

$(BIN): $(ELF)
	@echo --- make bin... 	
	$(OBJCOPY) -S -O binary $(ELF) $(BIN)
	
OSCore:
	make all -C $(OS_BASE_DIR) -f Makefile TOOL=$(TOOL) TOLLPATH=$(TOOLPATH) TOOLPOTFIX=$(TOOLPOTFIX) _NUL=$(_NUL) CHIP=$(CHIP) JENNIC_PCB=$(JENNIC_PCB) JENNIC_SDK_BASE_DIR=../../$(JENNIC_SDK_BASE_DIR)

VPATH := $(DIRS)

$(OBJDIR)/%.o: %.cpp
	@echo --- compiling $(*F).cpp
	$(CXX) -c $(CXXFLAGS) -o $@ $<

$(OBJDIR)/%.o: %.c
	@echo --- compiling $(*F).c
	$(CC) -c $(CFLAGS) -o $@ $<
#	$(CXX) -c $(CXXFLAGS) -o $@ $<

$(OBJDIR)/%.o: %.S
	@echo --- assembling $(*F).s
	$(AS) $(AFLAGS) -o $@ $<

dirs: $(OBJDIR) $(EXEDIR) $(LSTDIR) $(BAKDIR)

$(OBJDIR):
	-$(MD) $(OBJDIR)

$(EXEDIR):
	-$(MD) $(EXEDIR)

$(LSTDIR):
	-$(MD) $(LSTDIR)

$(BAKDIR):
	-$(MD) $(BAKDIR)

clean:
	@echo Cleaning all...
	@$(RM) $(OBJDIR)/*.d 2>$(_NUL)
	@$(RM) $(OBJDIR)/*.o 2>$(_NUL)
	@$(RM) $(EXEDIR)/*.hex 2>$(_NUL)
	@$(RM) $(EXEDIR)/*.bin 2>$(_NUL)
	@$(RM) $(EXEDIR)/*.elf 2>$(_NUL)
	@$(RM) $(LSTDIR)/*.lst 2>$(_NUL)
	@$(RM) $(LSTDIR)/*.lss 2>$(_NUL)
	@$(RM) $(LSTDIR)/*.map 2>$(_NUL)
	@echo Ready!

claenos:
	@echo --- Cleaning all os files
	make clean -C $(OS_BASE_DIR) -f Makefile TOOL=$(TOOL) TOLLPATH=$(TOOLPATH) TOOLPOTFIX=$(TOOLPOTFIX) _NUL=$(_NUL) CHIP=$(CHIP) JENNIC_PCB=$(JENNIC_PCB) JENNIC_SDK_BASE_DIR=../../$(JENNIC_SDK_BASE_DIR)

archive: OsSrcList
	@echo --- archiving...
ifeq ($(shell uname), windows32)
	$(RAR) a -agyy-mm-dd,hh-nn-ss $(BAKDIR)/$(TARGET)_$(TOOL)_.rar $(TARS)
else 
	$(TAR) -cvjf $(BAKDIR)/$(TARGET)_$(TOOL)_$(DATE).tar.bz2 $(TARS) $(shell cat $(OS_BASE_DIR)/OsFilesList.lst)
endif
	@echo --- done!
	
OsSrcList:
	make archive -C $(OS_BASE_DIR) -f Makefile TOOL=$(TOOL) TOLLPATH=$(TOOLPATH) TOOLPOTFIX=$(TOOLPOTFIX) _NUL=$(_NUL) CHIP=$(CHIP) JENNIC_PCB=$(JENNIC_PCB) JENNIC_SDK_BASE_DIR=../../$(JENNIC_SDK_BASE_DIR)

flash: all
	@echo Writing programm to Flas
	$(FLASHER) -f$(BIN) -c$(FLASHCOMPORT)
	@echo Ready!

rebuildall: clean OSCore all
	@echo Rebuild is compled!
	
rebuild: clean all
	@echo Rebuild is compled!
	
cu:
	@echo Opening terinal ttyDEV=$(ttyDEV), ttySPEED=$(ttySPEED)"
	cu -l $(ttyDEV) -s $(ttySPEED)
	
test:
	make test -C $(OS_BASE_DIR) -f Makefile TOOL=$(TOOL) TOLLPATH=$(TOOLPATH) TOOLPOTFIX=$(TOOLPOTFIX) _NUL=$(_NUL) CHIP=$(CHIP) JENNIC_PCB=$(JENNIC_PCB) JENNIC_SDK_BASE_DIR=../../$(JENNIC_SDK_BASE_DIR)
	
# dependencies
include $(wildcard $(OBJDIR)/*.d) 
