/*
 FreeRTOS.org V4.2.1 - Copyright (C) 2003-2007 Richard Barry.

 This file is part of the FreeRTOS.org distribution.

 FreeRTOS.org is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 FreeRTOS.org is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FreeRTOS.org; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 A special exception to the GPL can be applied should you wish to distribute
 a combined work that includes FreeRTOS.org, without being obliged to provide
 the source code for any proprietary components.  See the licensing section
 of http://www.FreeRTOS.org for full details of how and when the exception
 can be applied.

 ***************************************************************************
 See http://www.FreeRTOS.org for documentation, latest information, license
 and contact details.  Please ensure to read the configuration and relevant
 port sections of the online documentation.

 Also see http://www.SafeRTOS.com for an IEC 61508 compliant version along
 with commercial development and support options.
 ***************************************************************************
 */

/*
 Changes from V1.2.3

 + portCPU_CLOSK_HZ definition changed to 8MHz base 10, previously it
 base 16.
 */

#ifndef PORTMACRO_H
#define PORTMACRO_H

/*-----------------------------------------------------------
 * Port specific definitions.
 *
 * The settings in this file configure FreeRTOS correctly for the
 * given hardware and compiler.
 *
 * These settings should not be altered.
 *-----------------------------------------------------------
 */


#ifdef __cplusplus
extern "C" {
#endif

/* Type definitions. */
#define portCHAR		char
#define portFLOAT		float
#define portDOUBLE		double
#define portLONG		long
#define portSHORT		short
#define portSTACK_TYPE		unsigned int
#define portBASE_TYPE		int

/* NOTE: int and long are the same on or1200 architecture, both 32 bits */

#if( configUSE_16_BIT_TICKS == 1 )
typedef unsigned portSHORT portTickType;
#define portMAX_DELAY ( portTickType ) 0xffff
#else
typedef unsigned portLONG portTickType;
#define portMAX_DELAY ( portTickType ) 0xffffffff
#endif
/*-----------------------------------------------------------*/

/* Critical section management. */

/* Enable/disable external and tick timer interrupts */
#define portENABLE_INTERRUPTS();                                           	    \
    {                                                                      	    \
        register unsigned portLONG rulCtrlReg;                             	    \
        asm volatile ("l.mfspr %0, r0, 17;" :"=r"(rulCtrlReg) : );            	\
        rulCtrlReg |= 6;                                                   	    \
        asm volatile ("l.mtspr r0, %0, 17;" : :"r"(rulCtrlReg));           	    \
    }

#define portDISABLE_INTERRUPTS();                                               \
    {                                                                           \
        register unsigned portLONG rulCtrlReg;                             	    \
        asm volatile ("l.mfspr %0, r0, 17;" :"=r"(rulCtrlReg) : );              \
        rulCtrlReg &= 0xfffffff9;                                               \
        asm volatile ("l.mtspr r0, %0, 17;" : :"r"(rulCtrlReg));                \
    }

/* Critical section control macros. */
#define portNO_CRITICAL_SECTION_NESTING		( ( unsigned portSHORT ) 0 )

#define portENTER_CRITICAL()													\
{																				\
extern volatile unsigned portLONG ulCriticalNesting;							\
																				\
	portDISABLE_INTERRUPTS();													\
																				\
	/* Now interrupts are disabled ulCriticalNesting can be accessed */			\
	/* directly.  Increment ulCriticalNesting to keep a count of how many */	\
	/* times portENTER_CRITICAL() has been called. */							\
	ulCriticalNesting++;														\
}

#define portEXIT_CRITICAL()														\
{																				\
extern volatile unsigned portLONG ulCriticalNesting;							\
																				\
	if( ulCriticalNesting > portNO_CRITICAL_SECTION_NESTING )					\
	{																			\
		/* Decrement the nesting count as we are leaving a critical section. */	\
		ulCriticalNesting--;													\
																				\
		/* If the nesting level has reached zero then interrupts should be */	\
		/* re-enabled. */														\
		if( ulCriticalNesting == portNO_CRITICAL_SECTION_NESTING )				\
		{																		\
			portENABLE_INTERRUPTS();											\
		}																		\
	}																			\
}

/*-----------------------------------------------------------*/

/* Architecture specifics. */
#define portSTACK_GROWTH			( -1 )
#define portTICK_RATE_MS			( ( portTickType ) 1000 / configTICK_RATE_HZ )
#define portBYTE_ALIGNMENT			4
#define portNOP()				asm volatile ( "l.nop" );

/*-----------------------------------------------------------*/
#define portRESTORE_CONTEXT() \
    {\
    asm volatile (\
	    "l.movhi 	r3, hi(_pxCurrentTCB) \n\t"\
	    "l.ori   	r3,r3, lo(_pxCurrentTCB) \n\t"\
	    "l.lwz   	r3, 0x0(r3)\r\n"\
	    "l.lwz 	r1, 0x0(r3)\r\n"/*Восстановление значения указателя стека процесса*/\
	    "l.movhi 	r3, hi(_ulCriticalNesting)\r\n"\
	    "l.ori 	r3, r3, lo(_ulCriticalNesting)\r\n"\
	    "l.lwz 	r4, 0x0(r1)\r\n" /*0x0(r1) = ulCriticalNesting того процесса на который преключаемся*/\
	    "l.sw 	0x0(r3), r4\r\n"\
	    "l.lwz 	r2,2 * 4(r1)\r\n"\
	    "l.lwz 	r5,5 * 4(r1)\r\n"\
	    "l.lwz 	r6,6 * 4(r1)\r\n"\
	    "l.lwz 	r7,7 * 4(r1)\r\n"\
	    "l.lwz 	r8,8 * 4(r1)\r\n"\
	    "l.lwz 	r9,9 * 4(r1)\r\n"\
	    "l.lwz 	r10,10 * 4(r1)\r\n"\
	    "l.lwz 	r11,11 * 4(r1)\r\n"\
	    "l.lwz 	r12,12 * 4(r1)\r\n"\
	    "l.lwz	r13,13 * 4(r1)\r\n"\
	    "l.lwz	r14,14 * 4(r1)\r\n"\
	    "l.lwz 	r15,15 * 4(r1)\r\n"\
	    "l.lwz 	r16,16 * 4(r1)\r\n"\
	    "l.lwz 	r17,17 * 4(r1)\r\n"\
	    "l.lwz 	r18,18 * 4(r1)\r\n"\
	    "l.lwz 	r19,19 * 4(r1)\r\n"\
	    "l.lwz 	r20,20 * 4(r1)\r\n"\
	    "l.lwz 	r21,21 * 4(r1)\r\n"\
	    "l.lwz 	r22,22 * 4(r1)\r\n"\
	    "l.lwz 	r23,23 * 4(r1)\r\n"\
	    "l.lwz 	r24,24 * 4(r1)\r\n"\
	    "l.lwz 	r25,25 * 4(r1)\r\n"\
	    "l.lwz 	r26,26 * 4(r1)\r\n"\
	    "l.lwz 	r27,27 * 4(r1)\r\n"\
	    "l.lwz 	r28,28 * 4(r1)\r\n"\
	    "l.lwz 	r29,29 * 4(r1)\r\n"\
	    "l.lwz 	r30,30 * 4(r1)\r\n"\
	    "l.lwz	r31,31 * 4(r1)\r\n"\
	    /*MAC*/\
	    "l.lwz     	r4,0x84(r1)\r\n"\
	    "l.mtspr   	r0,r4,0x2801\r\n" /*MACLO*/\
	    "l.lwz     	r4,0x80(r1)\r\n"\
	    "l.mtspr   	r0,r4,0x2802\r\n" /*MACHI*/\
	    /* запрет прерываний на оставшийся кусок */\
	    "l.mfspr   	r3,r0,0x11\r\n"\
	    "l.movhi   	r4,0xffff\r\n"\
	    "l.ori     	r4,r4,0xfff9\r\n"\
	    "l.and     	r3,r4,r3\r\n"\
	    "l.mtspr   	r0,r3,0x11\r\n"\
	    /**/\
	    "l.lwz      r4,0x8c(r1)\r\n"\
	    "l.mtspr    r0,r4,0x40\r\n" /*ESR0*/\
	    "l.lwz      r4,0x90(r1)\r\n"\
	    "l.mtspr    r0,r4,0x20\r\n" /*EPCR0*/\
	    "l.lwz      r4,0x10(r1)\r\n"\
	    "l.lwz      r3,0xc(r1)\r\n"\
	    "l.lwz      r1,0x4(r1)\r\n"\
	    /*  return from interrupt*/\
	    "l.rfe	\r\n"\
	    "l.nop      0x0\r\n"\
	    );\
    }

#define portSAVE_CONTEXT() \
    {\
    asm volatile (\
	    "l.movhi 	r3, hi(_ulCriticalNesting) \r\n"\
	    "l.ori 	r3, r3, lo(_ulCriticalNesting) \r\n"\
	    "l.lwz 	r4,0x00(r3) \n\t"\
	    "l.sw 	0x00(r1),r4 \n\t" /*т.к. 0x00(r1) = 0, вместо него записываем значение ulCriticalNesting*/ \
	    "l.movhi 	r3, hi(_pxCurrentTCB) \n\t"\
	    "l.ori   	r3,r3, lo(_pxCurrentTCB) \n\t"\
	    "l.lwz   	r3, 0x0(r3)\r\n"\
	    "l.sw 	0x00(r3),r1 \r\n" /*сохранение значения SP (r1) в структуре управления текущим процессом pxCurrentTCB*/\
	    );\
    }

/* Kernel utilities. */
extern void vPortYield(void);
#define portYIELD()					vPortYield()
/*-----------------------------------------------------------*/

/* Task function macros as described on the FreeRTOS.org WEB site. */
#define portTASK_FUNCTION_PROTO( vFunction, pvParameters ) void vFunction( void *pvParameters )
#define portTASK_FUNCTION( vFunction, pvParameters ) void vFunction( void *pvParameters )

#ifdef __cplusplus
}
#endif

#endif /* PORTMACRO_H */
