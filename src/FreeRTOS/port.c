/*
 FreeRTOS.org V4.2.1 - Copyright (C) 2003-2007 Richard Barry.

 This file is part of the FreeRTOS.org distribution.

 FreeRTOS.org is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 FreeRTOS.org is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FreeRTOS.org; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 A special exception to the GPL can be applied should you wish to distribute
 a combined work that includes FreeRTOS.org, without being obliged to provide
 the source code for any proprietary components.  See the licensing section
 of http://www.FreeRTOS.org for full details of how and when the exception
 can be applied.

 ***************************************************************************
 See http://www.FreeRTOS.org for documentation, latest information, license
 and contact details.  Please ensure to read the configuration and relevant
 port sections of the online documentation.

 Also see http://www.SafeRTOS.com for an IEC 61508 compliant version along
 with commercial development and support options.
 ***************************************************************************
 */

/*
 Changes from V2.5.2

 + usCriticalNesting now has a volatile qualifier.
 */

/* Standard includes. */
#include <stdlib.h>
#include <signal.h>

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "portmacro.h"

/* Platform specific specific includes */
#include <AppHardwareApi.h>

//#define REPORT_EXCEPTION_TYPE

#ifdef __cplusplus
extern "C" {
#endif

/*------------------------------------------------------------------------
 * Implementation of functions defined in portable.h for the JN513x port.
 *------------------------------------------------------------------------*/

/* Constants required for hardware setup. */
#define portTICK_TIMER_CLK_FREQUENCY_HZ	( ( unsigned portLONG ) 16000000 )

#define portINITIAL_CRITICAL_NESTING	( ( unsigned portLONG ) 10 )

/* Tick Timer & External IRQ Enabled */
#define portFLAGS_INT_ENABLED	        ( ( portSTACK_TYPE ) 0x00000006 )

#define HAL_VSR_TABLE			( ( unsigned portLONG ) 0xf0017f80 )
#define UNKNOWNVECTORS
#define CATCH_ALL_HW_IRQ		0

#define IRQ_BUS_ERROR                   2 //ok
#define IRQ_PAGE_ERROR                  3 //?not present?
#define IRQ_CODE_PAGE_ERROR             4 //?not present?
#define IRQ_TICK_TIMER                  5 //ok
#define IRQ_ALIGNMENT_ERR               6 //ok
#define IRQ_ILL_INSTR                   7  //ok
#if CATCH_ALL_HW_IRQ == 1
#define IRQ_HARDWARE			8
#endif
#ifdef UNKNOWNVECTORS
#define IRQ_HZ9				9
#define IRQ_HZ10			0xa
#define IRQ_HZ11			0xb
#endif
#define IRQ_SYSTEM_CALL                 0xc //ok
#define IRQ_TRAP                        0xe //ok

#if CATCH_ALL_HW_IRQ == 1
static void vHWCatch(void);
#endif

#ifdef UNKNOWNVECTORS
static void vHZ9(void);
static void vHZa(void);
static void vHZb(void);
#endif

#define REPORT_EXCEPTION_PORT		E_AHI_UART_1
#define TIMER_START			1

/* We require the address of the pxCurrentTCB variable, but don't want to know
 any details of its type. */
typedef void tskTCB;
extern volatile tskTCB * volatile pxCurrentTCB;

//by Shilo_XyZ_
extern void vTickTimerHandler(void);
extern void vSystemCallHandler(void);

/*lint -e956 */

/* Most ports implement critical sections by placing the interrupt flags on
 the stack before disabling interrupts.  Exiting the critical section is then
 simply a case of popping the flags from the stack.  As JN51xx gcc does not use
 a frame pointer this cannot be done as modifying the stack will clobber all
 the stack variables.  Instead each task maintains a count of the critical
 section nesting depth.  Each time a critical section is entered the count is
 incremented.  Each time a critical section is left the count is decremented -
 with interrupts only being re-enabled if the count is zero.

 usCriticalNesting will get set to zero when the scheduler starts, but must
 not be initialised to zero as this will cause problems during the startup
 sequence. */
volatile unsigned portLONG ulCriticalNesting = portINITIAL_CRITICAL_NESTING;

/*-----------------------------------------------------------*/

static void prvSetupTimerInterrupt(void);
static void vInstallInterruptHandler(unsigned portCHAR ucIrqIdx,
	unsigned portLONG ulvFncAddr);

/* Run time exception handlers */
static void vBusErrorHandler(void);
static void vPageErrorHandler(void);
static void vCodePageErrorHandler(void);
static void vAlignmentErrorHandler(void);
static void vIllegalInstructionHandler(void);
static void vTrapHandler(void);
void vHandleException(unsigned portCHAR ucException);

#if configUSE_PREEMPTION == 0
static void prvTickTimerISR(unsigned portLONG ulDevice, unsigned portLONG ulItemBitmap);
#endif

/*-----------------------------------------------------------*/

portSTACK_TYPE *pxPortInitialiseStack(portSTACK_TYPE *pxTopOfStack,
	pdTASK_CODE pxCode, void *pvParameters)
    {
    /*
     * Initialise the stack of a task to look exactly as if a call to
     * portSAVE_CONTEXT had been called.
     *
     * See the header file portable.h.
     */

    portSTACK_TYPE xTopOfStack = (portSTACK_TYPE) pxTopOfStack;

    *pxTopOfStack = (portSTACK_TYPE ) 0x0000; /* EEAR +*/
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) pxCode; /* PC - EPCR0 + */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) portFLAGS_INT_ENABLED; /* SR +*/
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x5; /* Shilo_XyZ_ Always 5?? */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x0000; /* MACLO +*/
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x0000; /* MACHI +*/
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x001F; /* R31 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x001E; /* R30 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x001D; /* R29 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x001C; /* R28 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x001B; /* R27 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x001A; /* R26 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x0019; /* R25 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x0018; /* R24 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x0017; /* R23 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x0016; /* R22 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x0015; /* R21 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x0014; /* R20 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x0013; /* R19 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x0012; /* R18 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x0011; /* R17 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x0010; /* R16 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x000F; /* R15 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x000E; /* R14 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x000D; /* R13 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x000C; /* R12 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x000B; /* R11 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x000A; /* R10 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x0009; /* R9 (LR)*/
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x0008; /* R8 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x0007; /* R7 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x0006; /* R6 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x0005; /* R5 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x0004; /* R4 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) pvParameters; /* R3 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) 0x0002; /* R2 */
    pxTopOfStack--;
    *pxTopOfStack = (portSTACK_TYPE ) xTopOfStack; /* R1 (SP) */
    pxTopOfStack--;
    /*
     *pxTopOfStack = (portSTACK_TYPE ) 0x0000; // R0
     pxTopOfStack--;
     */
    *pxTopOfStack = portNO_CRITICAL_SECTION_NESTING;

    /* Return a pointer to the top of the stack we have generated so this can
     be stored in the task control block for the task. */
    return pxTopOfStack;
    }
/*-----------------------------------------------------------*/

portBASE_TYPE xPortStartScheduler(void)
    {
    /* Setup the hardware to generate the tick.  Interrupts are disabled when
     this function is called. */
    prvSetupTimerInterrupt();

    /* Install interrupt handler for system call exception, this will allow
     us to yield manually */
    vInstallInterruptHandler(IRQ_SYSTEM_CALL,
	    (unsigned portLONG) vSystemCallHandler);

    /* Install handlers for run-time exceptions */
    vInstallInterruptHandler(IRQ_BUS_ERROR,
	    (unsigned portLONG) vBusErrorHandler);
    vInstallInterruptHandler(IRQ_PAGE_ERROR,
	    (unsigned portLONG) vPageErrorHandler);
    vInstallInterruptHandler(IRQ_CODE_PAGE_ERROR,
	    (unsigned portLONG) vCodePageErrorHandler);
    vInstallInterruptHandler(IRQ_ALIGNMENT_ERR,
	    (unsigned portLONG) vAlignmentErrorHandler);
    vInstallInterruptHandler(IRQ_ILL_INSTR,
	    (unsigned portLONG) vIllegalInstructionHandler);

#if CATCH_ALL_HW_IRQ == 1
    vInstallInterruptHandler(IRQ_HARDWARE, (unsigned portLONG) vHWCatch);
#endif

#ifdef UNKNOWNVECTORS
    vInstallInterruptHandler(IRQ_HZ9, (unsigned portLONG) vHZ9);
    vInstallInterruptHandler(IRQ_HZ10, (unsigned portLONG) vHZa);
    vInstallInterruptHandler(IRQ_HZ11, (unsigned portLONG) vHZb);
#endif

#ifndef GDB
    vInstallInterruptHandler(IRQ_TRAP, (unsigned portLONG) vTrapHandler);
#endif
    /* Restore the context of the first task that is going to run. */

    portRESTORE_CONTEXT(); //неаккуратненькао... *FIXME*

    /* Should not get here as the tasks are now running! */
    return pdTRUE;
    }
/*-----------------------------------------------------------*/

void vPortEndScheduler(void)
    {
    /* It is unlikely that the JN51xx port will get stopped.
     If required simply disable the tick interrupt here. */
    }

/*-----------------------------------------------------------*/

/*
 * Manual context switch called by portYIELD or taskYIELD.
 *
 */
void vPortYield(void)
    {
    /* We want the stack of the task being saved to look exactly as if the task
     was saved during a pre-emptive RTOS tick ISR.  */

    asm volatile ( "l.sys   0x0");
    asm volatile ( "l.nop   0x0");
    }
/*-----------------------------------------------------------*/

/*
 * Sets up the periodic ISR used for the RTOS tick.  This uses the tick
 * timer but could have alternatively used the timer 0 or timer 1.
 */
static void prvSetupTimerInterrupt(void)
    {
    /* Initialise tick timer */
    vAHI_TickTimerConfigure(E_AHI_TICK_TIMER_DISABLE);
    vAHI_TickTimerWrite(0);

#if configUSE_PREEMPTION == 0
    vAHI_TickTimerInit(prvTickTimerISR);
#else

    /* Install custom interrupt handler */
    /*
     vInstallInterruptHandler(IRQ_TICK_TIMER, (unsigned portLONG)vTickTimerHandler); //является ли вектор IRQ_TICK_TIMER == 5 от таймера ведь это для 5139
     */
    vAHI_TickTimerInit(NULL);
    vInstallInterruptHandler(IRQ_TICK_TIMER,
	    (unsigned portLONG) vTickTimerHandler);
#endif
    vAHI_TickTimerInterval(
	    portTICK_TIMER_CLK_FREQUENCY_HZ / configTICK_RATE_HZ);
    vAHI_TickTimerConfigure(E_AHI_TICK_TIMER_RESTART);
#if TIMER_START == 1
    vAHI_TickTimerIntEnable(TRUE);
    portDISABLE_INTERRUPTS();
#endif
    }

/*-----------------------------------------------------------*/
static void vInstallInterruptHandler(unsigned portCHAR ucIrqIdx,
	unsigned portLONG ulvFncAddr)
    {
    unsigned portLONG **ppulInterruptHandlerPointer;

    ppulInterruptHandlerPointer = (unsigned portLONG **) (HAL_VSR_TABLE
	    + ((0x100 * ucIrqIdx) >> 6)); //ok
    *ppulInterruptHandlerPointer = (unsigned portLONG *) ulvFncAddr; //ok
    }

/*-----------------------------------------------------------*/

#if configUSE_PREEMPTION == 0

/*
 * Tick ISR for the cooperative scheduler.  All this does is increment the
 * tick count.  We don't need to switch context, this can only be done by
 * manual calls to taskYIELD();
 */
static void prvTickTimerISR(unsigned portLONG ulDevice, unsigned portLONG ulItemBitmap)
    {
    vTaskIncrementTick();
    }

#endif

/*-----------------------------------------------------------*/

static void vBusErrorHandler(void)
    {
    vHandleException('B');
    }

/*-----------------------------------------------------------*/

static void vPageErrorHandler(void)
    {
    vHandleException('P');
    }

/*-----------------------------------------------------------*/

static void vCodePageErrorHandler(void)
    {
    vHandleException('C');
    }

/*-----------------------------------------------------------*/

static void vAlignmentErrorHandler(void)
    {
    vHandleException('A');
    }

/*-----------------------------------------------------------*/

static void vIllegalInstructionHandler(void)
    {
    vHandleException('I');
    }

/*-----------------------------------------------------------*/

static void vHWCatch(void)
    {
    vHandleException('H');
    }

/*-----------------------------------------------------------*/

static void vHZ9(void)
    {
    vHandleException('9');
    }

/*-----------------------------------------------------------*/

static void vHZa(void)
    {
    vHandleException('a');
    }

/*-----------------------------------------------------------*/

static void vHZb(void)
    {
    vHandleException('b');
    }

/*-----------------------------------------------------------*/

static void vTrapHandler(void)
    {
    vHandleException('T');
    }

/*-----------------------------------------------------------*/

void vHandleException(unsigned portCHAR ucException)
    {
#ifdef REPORT_EXCEPTION_TYPE

    /* Enable and reset required UART */
    vAHI_UartEnable(REPORT_EXCEPTION_PORT);
    vAHI_UartReset(REPORT_EXCEPTION_PORT, TRUE, TRUE);
    vAHI_UartReset(REPORT_EXCEPTION_PORT, FALSE, FALSE);

    vAHI_UartSetClockDivisor(REPORT_EXCEPTION_PORT, E_AHI_UART_RATE_115200);

    vAHI_UartSetControl(REPORT_EXCEPTION_PORT, E_AHI_UART_EVEN_PARITY,
	    E_AHI_UART_PARITY_DISABLE, E_AHI_UART_WORD_LEN_8,
	    E_AHI_UART_1_STOP_BIT, E_AHI_UART_RTS_LOW);

    vAHI_UartSetInterrupt(REPORT_EXCEPTION_PORT, FALSE, FALSE, FALSE, FALSE,
	    E_AHI_UART_FIFO_LEVEL_1);

    while ((u8AHI_UartReadLineStatus(REPORT_EXCEPTION_PORT) & E_AHI_UART_LS_THRE)
	    == 0)
	;
    vAHI_UartWriteData(REPORT_EXCEPTION_PORT, ucException);
#if STOP_ON_EXCEPTION
    while(1);
#endif

#endif
    vAHI_SwReset();
    }

#ifdef __cplusplus
}
#endif

