/*
 * portISR.c
 *
 *  Created on: 25.10.2011
 *      Author: shiloxyz
 */

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "portmacro.h"

#ifdef __cplusplus
extern "C" {
#endif

// вызвается при "l.sys"
void vSystemCallHandler(void)
    {
    // откат в предидущий кадр стека
#if GDB
    asm volatile (
	    "l.addi	r1,r1,0x4\n\r"
    );
#else
    asm volatile (
	    "l.addi	r1,r1,0x4\n\r"
    );
#endif
    portDISABLE_INTERRUPTS();
    portSAVE_CONTEXT();
    vTaskSwitchContext();
    portRESTORE_CONTEXT();
    }

// вызывается системным таймером
void vTickTimerHandler(void)
    {
    // откат в предидущий кадр стека
#if GDB
    asm volatile (
	    "l.addi	r1,r1,0x4\n\r"
    );
#else
    asm volatile (
	    "l.addi	r1,r1,0x4\n\r"
    );
#endif
    portDISABLE_INTERRUPTS();
    portSAVE_CONTEXT();
    vTaskSwitchContext();
    vTaskIncrementTick();
    // вырубание флага IP
    asm volatile (
	    "l.mfspr 	r3, r0, 0x5000\n\r"
	    "l.movhi	r4, hi(0xefffffff)\n\r"
	    "l.ori   	r4, r4, lo(0xefffffff)\n\r"
	    "l.and   	r3, r3, r4\n\r"
	    "l.mtspr 	r0, r3, 0x5000\n\r"
    );
    portRESTORE_CONTEXT();
    }

#ifdef __cplusplus
}
#endif
