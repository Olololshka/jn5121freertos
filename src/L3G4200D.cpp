#include <L3G4200D.h>

#include <math.h>
#include <errno.h>

#include "CppIncs.hpp"
#include "common.h"
#include "SPIBackend.hpp"


extern hwSPI JN5121SPI;

// Defines ////////////////////////////////////////////////////////////////

// The Arduino two-wire interface uses a 7-bit number for the address,
// and sets the last bit correctly based on reads and writes
#define GYR_ADDRESS (0xD2 >> 1)

// Public Methods //////////////////////////////////////////////////////////////

L3G4200D::L3G4200D()
    {

    }

// Turns on the L3G4200D's gyro and places it in normal mode.
void L3G4200D::enableDefault(void)
    {
    // 0x0F = 0b00001111
    // Normal power mode, all axes enabled
    writeReg(L3G4200D_CTRL_REG1, 0x0F);
    }

// Writes a gyro register
void L3G4200D::writeReg(char reg, char value)
    {
    JN5121SPI.enStart(L3G4200D_SS_PIN);
    JN5121SPI.ucTransfer8(BIN8(00111111) & reg);
    JN5121SPI.ucTransfer8(value);
    JN5121SPI.enStop();
    }

// Reads a gyro register
char L3G4200D::readReg(char reg)
    {
    char value;
    JN5121SPI.enStart(L3G4200D_SS_PIN);
    JN5121SPI.ucTransfer8((1 << 7) | reg);
    value = JN5121SPI.ucTransfer8(0x00);
    JN5121SPI.enStop();
    return value;
    }

// Reads the 3 gyro channels and stores them in vector g
void L3G4200D::read()
    {
    char rawdata[6];
    JN5121SPI.enStart(L3G4200D_SS_PIN);
    JN5121SPI.ucTransfer8((1 << 7) | (1 << 6) | GYR_ADDRESS); //начальный адрес + MultyByte Read Mode
    JN5121SPI.enTransferBuff(rawdata, sizeof(rawdata)); //читаем 6 байт
    JN5121SPI.enStop();

    g.x = rawdata[1] << 8 | rawdata[0];
    g.y = rawdata[3] << 8 | rawdata[2];
    g.z = rawdata[5] << 8 | rawdata[4];
    }

void L3G4200D::vector_cross(const vector *a, const vector *b, vector *out)
    {
    out->x = a->y * b->z - a->z * b->y;
    out->y = a->z * b->x - a->x * b->z;
    out->z = a->x * b->y - a->y * b->x;
    }

float L3G4200D::vector_dot(const vector *a, const vector *b)
    {
    return a->x * b->x + a->y * b->y + a->z * b->z;
    }

void L3G4200D::vector_normalize(vector *a)
    {
    float mag = sqrt(vector_dot(a, a));
    a->x /= mag;
    a->y /= mag;
    a->z /= mag;
    }

