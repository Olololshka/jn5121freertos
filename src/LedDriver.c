/*
 * LedDriver.c
 *
 *  Created on: 20.10.2011
 *      Author: shiloxyz
 */

#include <stdlib.h>
#include <AppHardwareApi.h>

#include "common.h"
#include "portmacro.h"

#if defined __cplusplus
extern "C" {
#endif

void vLedInit(unsigned portLONG shMask)
    {
    vAHI_DioSetDirection(0, shMask); //по маске на выход
    vAHI_DioSetOutput(shMask, 0); //вырубить (1)
    }

void vLedTougle(unsigned portLONG shMask)
    {
    unsigned portLONG ulLastState = u32AHI_DioReadInput(); //читаем текущее состояние пинов
    _TOUGLE_FLAG(ulLastState, shMask); //накладываем маску на инвертированные состояния, чтобы отсеч то что не светолиод
    vAHI_DioSetOutput(ulLastState, ~ulLastState); // запись новых состояний
    }

#if defined __cplusplus
}
#endif
