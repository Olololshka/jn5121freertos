/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/

#include <stdarg.h>
#include <ctype.h>
#include "Printf.h"

#include <AppHardwareApi.h>

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#include "common.h"

#ifdef __cplusplus
extern "C"
    {
#endif

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/

void vNum2String(void(*pfPutCharFun)(char), unsigned long u32Number,
	unsigned char u8Base);
void vPutsToBufReset(char* pcNewBuf);
char* vPutsToBufTERMINATE(char* pcNewBuf);
void vPutsToBuf(char cChar);
void vUARTReset(unsigned char ucPort, unsigned char ucBoud);
unsigned long ulU_Printf(void(*pfPutCharFun)(char), const char *fmt, va_list ap);

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

char* pcPutsBuf = NULL;
bool uxIsUartInited = FALSE;

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

unsigned long portSPrintf(char* pfBuf, const char* pcFormat, ...)
    { //threadunsafe
    va_list varList;
    va_start(varList, pcFormat);
    vPutsToBufReset(pfBuf);
    ulU_Printf(vPutsToBuf, pcFormat, varList); // предается поле pcFormat и все остальное
    va_end(varList);
    return (unsigned long) vPutsToBufTERMINATE(pfBuf) - (unsigned long) pfBuf; //длина записи
    }

unsigned long portPrintf(const char *pcFormat, ...)
    {
    static xSemaphoreHandle xPrintfMutex = NULL;
    if (xPrintfMutex == NULL) // если мьютекс не создан,
	xPrintfMutex = xQueueCreateMutex(); // создаем его
    _MUTEX_TAKE(xPrintfMutex); // пытаемся схватить мьютекс
    unsigned long ulResult;
    va_list varList;
    va_start(varList, pcFormat);
    ulResult = ulU_Printf(vPutsToUART, pcFormat, varList); // предается поле pcFormat и все остальное
    va_end(varList);
    _MUTEX_GIVE(xPrintfMutex);
    return ulResult;
    }

void vPutsToUART(char cChar)
    {
    if (!uxIsUartInited)
	vUARTReset(UARTPORT, BAUD);
    while ((u8AHI_UartReadLineStatus(UARTPORT) & E_AHI_UART_LS_THRE) == 0)
	;
    vAHI_UartWriteData(UARTPORT, cChar);
    }

/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

unsigned long ulU_Printf(void(*pfPutCharFun)(char), const char *fmt, va_list ap)
    {
    char *bp = (char*) fmt;
    //va_list ap;
    char c;
    char *p;
    int32 i;

    //va_start(ap, fmt);

    while ((c = *bp++))
	{
	if (c != '%')
	    {
	    if (c == '\n')
		{
		pfPutCharFun('\n');
		pfPutCharFun('\r');
		}
	    else
		{
		pfPutCharFun(c);
		}
	    continue;
	    }

	switch ((c = *bp++))
	    {

	/* %d - show a decimal value */
	case 'u':
	    vNum2String(pfPutCharFun, va_arg(ap, uint32), 10);
	    break;

	    /* %x - show a value in hex */
	case 'x':
	    pfPutCharFun('0');
	    pfPutCharFun('x');
	    vNum2String(pfPutCharFun, va_arg(ap, uint32), 16);
	    break;

	    /* %b - show a value in binary */
	case 'b':
	    pfPutCharFun('0');
	    pfPutCharFun('b');
	    vNum2String(pfPutCharFun, va_arg(ap, uint32), 2);
	    break;

	    /* %c - show a character */
	case 'c':
	    pfPutCharFun(va_arg( ap, int));
	    break;

	case 'i':
	    i = va_arg(ap, int32);
	    if (i < 0)
		{
		pfPutCharFun('-');
		vNum2String(pfPutCharFun, (~i) + 1, 10);
		}
	    else
		{
		vNum2String(pfPutCharFun, i, 10);
		}
	    break;

	    /* %s - show a string */
	case 's':
	    p = va_arg(ap, char*);
	    do
		{
		pfPutCharFun(*p++);
		}
	    while (*p);
	    break;

	    /* %% - show a % character */
	case '%':
	    pfPutCharFun('%');
	    break;

	    /* %something else not handled ! */
	default:
	    pfPutCharFun('?');
	    break;

	    }
	}

    return (unsigned long) bp - (unsigned long) fmt; // How many bytes processed?
    }

/*
 * vNum2String()
 *  Convert a number to a string
 */
void vNum2String(void(*pfPutCharFun)(char), unsigned long u32Number,
	unsigned char u8Base)
    {
    char buf[33];
    char *p = buf + 33;
    unsigned long c, n;

    *--p = '\0';
    do
	{
	n = u32Number / u8Base;
	c = u32Number - (n * u8Base);
	if (c < 10)
	    {
	    *--p = '0' + c;
	    }
	else
	    {
	    *--p = 'a' + (c - 10);
	    }
	u32Number /= u8Base;
	}
    while (u32Number != 0);

    while (*p)
	{
	pfPutCharFun(*p);
	p++;
	}
    return;
    }

void vPutsToBufReset(char* pcNewBuf)
    {
    pcPutsBuf = pcNewBuf;
    }

char* vPutsToBufTERMINATE(char* pcNewBuf)
    {
    char* pcResult = pcPutsBuf;
    pcPutsBuf = NULL;
    return pcResult;
    }

void vPutsToBuf(char cChar)
    {
    if (pcPutsBuf != NULL)
	{
	*pcPutsBuf = cChar;
	++pcPutsBuf;
	}
    }

void vUARTReset(unsigned char ucPort, unsigned char ucBoud)
    {
    vAHI_UartEnable(ucPort);
    vAHI_UartReset(ucPort, TRUE, TRUE);
    vAHI_UartReset(ucPort, FALSE, FALSE);
    vAHI_UartSetClockDivisor(ucPort, ucBoud);
    vAHI_UartSetControl(ucPort, E_AHI_UART_EVEN_PARITY,
	    E_AHI_UART_PARITY_DISABLE, E_AHI_UART_WORD_LEN_8,
	    E_AHI_UART_1_STOP_BIT, E_AHI_UART_RTS_LOW);
    vAHI_UartSetInterrupt(ucPort, FALSE, FALSE, FALSE, FALSE, FIFOLVL);
    uxIsUartInited = TRUE;
    }

#ifdef __cplusplus
}
#endif

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/

