/*
 * SPIbackend.cpp
 *
 *  Created on: 08.12.2011
 *      Author: tolyan
 */

/* Platform Includes */
#include <AppApi.h>
#include <AppHardwareApi.h>
#include <stdbool.h>

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

#include "SPIBackend.hpp"
#include "common.h"

hwSPI::hwSPI()
    {
    vReset();
    }

void hwSPI::vReset(void)
    {
    if (xhwSPIMutex == NULL)
	xhwSPIMutex = xSemaphoreCreateMutex();
    else
	_MUTEX_GIVE(xhwSPIMutex);

    // установка значений по-умолчанию
    bLsbFirst = SPICONFIG_LSB_FIRST_DEFAULT;
    bTxNegEdge = SPICONFIG_TX_NEG_DEFAULT;
    bRxNegEdge = SPICONFIG_RX_NEG_DEFAULT;
    ucClockDivider = SPICONFIG_CLOCK_DIVIDER_DEFAULT;
    ucCurrentCS = 0;
    ulUsedCSMask = SPICONFIG_CS_MASK_DEFAULT;

    // включение SS'ов навыход
    vAHI_DioSetDirection(0, ulUsedCSMask);
    vAHI_DioSetOutput(ulUsedCSMask, 0);
    }

enum enhwSPI_ERRORS hwSPI::enSetDataEdges(bool bTxNegEdge, bool bRxNegEdge)
    {
    xTaskHandle xCurentTaskHandle = xTaskGetCurrentTaskHandle();
    if ((xCurentTaskHandle != xhwSPIOwner) || (xhwSPIOwner == NULL))
	return SPI_ERR_OWNER;
    else
	{
	this->bRxNegEdge = bRxNegEdge;
	this->bTxNegEdge = bTxNegEdge;
	return SPI_ERR_OK;
	}
    }

enum enhwSPI_ERRORS hwSPI::enSetLsbFirst(bool bLsbFirst)
    {
    xTaskHandle xCurentTaskHandle = xTaskGetCurrentTaskHandle();
    if ((xCurentTaskHandle != xhwSPIOwner) || (xhwSPIOwner == NULL))
	return SPI_ERR_OWNER;
    else
	{
	this->bLsbFirst = bLsbFirst;
	return SPI_ERR_OK;
	}
    }

enum enhwSPI_ERRORS hwSPI::enSetClockDivider(unsigned char ucClockDivider)
    {
    xTaskHandle xCurentTaskHandle = xTaskGetCurrentTaskHandle();
    if ((xCurentTaskHandle != xhwSPIOwner) || (xhwSPIOwner == NULL))
	return SPI_ERR_OWNER;
    else
	{
	this->ucClockDivider = ucClockDivider;
	return SPI_ERR_OK;
	}
    }

enum enhwSPI_ERRORS hwSPI::enStart(unsigned long ulCS)
    {
    xTaskHandle xCurentTaskHandle = xTaskGetCurrentTaskHandle();
    if (xCurentTaskHandle == xhwSPIOwner)
	return SPI_ERR_ALLREADY_STARTED;
    if (!(ulUsedCSMask & ulCS))
	return SPI_ERR_INVALID_CS;

    _MUTEX_TAKE(xhwSPIMutex);
    xhwSPIOwner = xCurentTaskHandle;
    vAHI_SpiConfigure(0, //не управлять SS
	    bLsbFirst, bTxNegEdge, bRxNegEdge, ucClockDivider, // настройки
	    0, // не включать прерывание
	    0 // отключить автослейвселект
	    );
    ucCurrentCS = ulCS; // Запись текущего CS
    vAHI_DioSetOutput(0, ulCS); // включить сам CS -> 0
    return SPI_ERR_OK;
    }

enum enhwSPI_ERRORS hwSPI::enStop(void)
    {
    xTaskHandle xCurentTaskHandle = xTaskGetCurrentTaskHandle();
    if (xCurentTaskHandle == xhwSPIOwner)
	{ // действительный путь
	_MUTEX_GIVE(xhwSPIMutex);
	ucCurrentCS = 0;
	xhwSPIOwner = NULL;
	vAHI_DioSetOutput(ulUsedCSMask, 0); // вырубить все
	}
    if (xhwSPIOwner == NULL)
	{ //если никто и не открывал
	ucCurrentCS = 0;
	vAHI_DioSetOutput(ulUsedCSMask, 0); // вырубить все
	return SPI_ERR_STOP_THEWOUT_START;
	}
    return SPI_ERR_OWNER; // не имееш права
    }

unsigned char hwSPI::ucTransfer8(unsigned char cByteToSend)
    {
    xTaskHandle xCurentTaskHandle = xTaskGetCurrentTaskHandle();
    if (xCurentTaskHandle == xhwSPIOwner)
	{
	unsigned char ucRes;
	vAHI_SpiStartTransfer8(cByteToSend);
	vAHI_SpiWaitBusy();
	ucRes = u8AHI_SpiReadTransfer8();
	return ucRes;
	}
    else
	return SPI_ERR_OWNER;
    }

enum enhwSPI_ERRORS hwSPI::enTransferBuff(char* pucBuff, unsigned long ulLength)
    {
    xTaskHandle xCurentTaskHandle = xTaskGetCurrentTaskHandle();
    if (xCurentTaskHandle == xhwSPIOwner)
	{
	unsigned long ulI;
	for (ulI = 0; ulI < ulLength; ++ulI)
	    {
	    vAHI_SpiStartTransfer8(pucBuff[ulI]);
	    vAHI_SpiWaitBusy();
	    pucBuff[ulI] = u8AHI_SpiReadTransfer8();
	    }
	return SPI_ERR_OK;
	}
    else
	return SPI_ERR_OWNER;
    }

hwSPI JN5121SPI;
