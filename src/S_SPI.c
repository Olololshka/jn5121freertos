//Lord_A
//********************************************************************************************
// ������ �������� ������� ������ � ����������� SPI � ������ ������ (����������� ����������) *
//********************************************************************************************

#include <AppApi.h>
#include <AppHardwareApi.h>

#include "S_SPI.h"
//#include "Delay.h"
#include "common.h"

void prgSPI_Init(spi_str* s)
    {
    //SCS |= 1;

    vAHI_DioSetDirection(SSPI_MISO, SSPI_SS | SSPI_M0SI | SSPI_SCK);
    //IO_MISO_DIR	(PORT_IN_STATE);
    //IO_MOSI_DIR	(PORT_OUT_STATE);
    //IO_SCK_DIR	(PORT_OUT_STATE);
    }

char prgSPI_MasterExchange(spi_str* s, char tx_data)
    {
    register char i;
    register char rx_data = 0;
    register char mask;
    register char _dord_ = s->_dord_;
    register char _cpol_ = s->_cpol_;
    register char _cpha_ = s->_cpha_;
    //register WORD delay_us = s->delay_us;

    if (_dord_)
	{
	mask = 0x01;
	}
    else
	{
	mask = 0x80;
	}

    if (_cpha_)
	{
	if (_cpol_)
	    {
	    IO_SCK_1;
	    }
	else
	    {
	    IO_SCK_0;
	    }
	//Delay_us(delay_us);
	for (i = 0; i < 8; i++)
	    {
	    if (_cpol_)
		{
		IO_SCK_0;
		}
	    else
		{
		IO_SCK_1;
		}
	    //Delay_us(delay_us / 2);
	    if (tx_data & mask)
		{
		IO_MOSI_1;
		}
	    else
		{
		IO_MOSI_0;
		}
	    //Delay_us(delay_us / 2);
	    if (_dord_)
		{
		tx_data >>= 1;
		}
	    else
		{
		tx_data <<= 1;
		}
	    if (_cpol_)
		{
		IO_SCK_1;
		}
	    else
		{
		IO_SCK_0;
		}
	    //Delay_us(delay_us);
	    if (_dord_)
		{
		if (IO_MISO_READ)
		    {
		    rx_data |= (1 << i);
		    }
		}
	    else
		{
		if (IO_MISO_READ)
		    {
		    rx_data |= (1 << (7 - i));
		    }
		}
	    }
	}
    else
	{
	for (i = 0; i < 8; i++)
	    {
	    if (_cpol_)
		{
		IO_SCK_1;
		}
	    else
		{
		IO_SCK_0;
		}
	    //Delay_us(delay_us);
	    if (tx_data & mask)
		{
		IO_MOSI_1;
		}
	    else
		{
		IO_MOSI_0;
		}
	    //Delay_us(delay_us / 2);
	    if (_dord_)
		{
		tx_data >>= 1;
		}
	    else
		{
		tx_data <<= 1;
		}
	    if (_cpol_)
		{
		IO_SCK_0;
		}
	    else
		{
		IO_SCK_1;
		}
	    //Delay_us(delay_us / 2);
	    if (_dord_)
		{
		if (IO_MISO_READ)
		    {
		    rx_data |= (1 << i);
		    }
		}
	    else
		{
		if (IO_MISO_READ)
		    {
		    rx_data |= (1 << (7 - i));
		    }
		}
	    }
	if (_cpol_)
	    {
	    IO_SCK_1;
	    }
	else
	    {
	    IO_SCK_0;
	    }
	//Delay_us(delay_us);
	}
    return rx_data;
    }

spi_str sSpi =
    {
    NULL,
	{
	NULL, NULL, NULL, NULL, 1
	},
	{
	NULL, NULL, NULL, NULL, 2
	},
	{
	NULL, NULL, NULL, NULL, 3
	}, 0, ._dord_ = 0, ._cpol_ = 1, ._cpha_ = 1
    };
