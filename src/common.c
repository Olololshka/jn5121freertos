/* common.c */

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

#include "common.h"

//----------------------------------------------------------
void* pvMyRealloc(void* pvOldPlace, size_t xNewSize)
    {
    void* pvNewPlace = NULL;
    if (pvOldPlace != NULL)
	if ((pvNewPlace = _malloc(xNewSize)) != NULL)
	    {
	    memcpy(pvNewPlace, pvOldPlace, xNewSize);
	    vPortFree(pvOldPlace);
	    }
    return pvNewPlace;
    }

signed long slStr2LongAuto(char* pcString)
    {
    bool fResNegative;
    switch (*pcString)
	// если в начале строки обнаружен -, то возвращать будем значение с минусом
	{
    case '-':
	fResNegative = 1;
	++pcString; //сдвигаем указатель далее
	break;
    case '+':
	++pcString;//сдвигаем указатель далее
    default:
	fResNegative = 0;
	break;
	}
    //определяем основание
    unsigned char ucBase = 10; //по-умолчанию;
    if (pcString[0] == '0') //детектируем модификатор системы счисления если он есть то система не 10
	switch (pcString[1])
	    {
	case 'x':
	case 'X':
	    ucBase = 16; //hex
	    pcString += 2; //само число начнется далее на 2 знака (0x)
	    break;
	case 'b':
	case 'B':
	    ucBase = 2; //bin
	    pcString += 2;//само число начнется далее на 2 знака (0b)
	    break;
	default:
	    ucBase = 8; //oct
	    pcString += 1; //само число начнется далее на 2 знака (0)
	    break;
	    }
    return (fResNegative) ? (-strtol(pcString, NULL, ucBase)) : (strtol(pcString, NULL, ucBase));
    }

unsigned long ulStr2ULongAuto(char* pcString)
    {
    //определяем основание
    unsigned char ucBase = 10; //по-умолчанию;
    if (pcString[0] == '0') //детектируем модификатор системы счисления если он есть то система не 10
	switch (pcString[1])
	    {
	case 'x':
	case 'X':
	    ucBase = 16; //hex
	    pcString += 2; //само число начнется далее на 2 знака (0x)
	    break;
	case 'b':
	case 'B':
	    ucBase = 2; //bin
	    pcString += 2;//само число начнется далее на 2 знака (0b)
	    break;
	default:
	    ucBase = 8; //oct
	    pcString += 1; //само число начнется далее на 2 знака (0)
	    break;
	    }
    //--------------
    unsigned long cutoff = 0xffffffff;
    int cutlim = cutoff % ucBase;
    cutoff /= ucBase;
    //--------------
    char cChar = *pcString++; //конвертируемый символ
    unsigned long acc; //результат
    signed char any; //флаг перебора
    for (acc = 0, any = 0;; cChar = *pcString++) //читаем символ
	{
	if ((cChar >= '0') && (cChar <= '9')) //символ - число?
	    cChar -= '0';
	else
	    {
	    if ((cChar >= 'A') && (cChar <= 'Z')) // ..мб большая буква
		cChar -= 'A' - 10;
	    else
		{
		if ((cChar >= 'a') && (cChar <= 'z')) //..мб маленькая буква
		    cChar -= 'a' - 10;
		else
		    break;
		}
	    }
	if (cChar >= ucBase) // с теперь число, должно быть не больше базы-1
	    break;
	if (any < 0 || acc > cutoff || (acc == cutoff && cChar > cutlim))
	    any = -1; //признак перебора
	else
	    {
	    any = 1; //все ок
	    acc *= ucBase;
	    acc += cChar;
	    }
	}
    if (any < 0)
	acc = 0xffffffff;
    return acc;
    //------------------
    }

int *__errno(void)
    {
    return 0;
    }
