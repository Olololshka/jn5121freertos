/*
 * CppIncs.hpp
 *
 *  Created on: 08.12.2011
 *      Author: tolyan
 */

#ifndef CPPINCS_HPP_
#define CPPINCS_HPP_

#define BIN8(x) BIN___(0##x)
#define BIN___(x)                                        \
	(                                                \
	((x / 01ul) % 010)*(2>>1) +                      \
	((x / 010ul) % 010)*(2<<0) +                     \
	((x / 0100ul) % 010)*(2<<1) +                    \
	((x / 01000ul) % 010)*(2<<2) +                   \
	((x / 010000ul) % 010)*(2<<3) +                  \
	((x / 0100000ul) % 010)*(2<<4) +                 \
	((x / 01000000ul) % 010)*(2<<5) +                \
	((x / 010000000ul) % 010)*(2<<6)                 \
	)

#endif /* CPPINCS_HPP_ */
