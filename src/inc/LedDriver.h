/*
 * LedDriver.h
 *
 *  Created on: 20.10.2011
 *      Author: shiloxyz
 */

#ifndef _LEDDRIVER_H_
#define _LEDDRIVER_H_

#ifdef __cplusplus
extern "C" {
#endif

void vLedInit(unsigned portLONG shMask);
void vLedTougle(unsigned portLONG shMask);

#ifdef __cplusplus
}
#endif

#endif /* LEDDRIVER_H_ */
