#ifndef PRINTF_H_INCLUDED
#define PRINTF_H_INCLUDED

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/

#include <jendefs.h>

/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

#if defined __cplusplus
extern "C" {
#endif

#define UARTPORT	E_AHI_UART_1
#define BAUD		E_AHI_UART_RATE_115200
#define FIFOLVL		E_AHI_UART_FIFO_LEVEL_14

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

unsigned long portPSrintf(char* pfBuf, const char* pcFormat, ...);
unsigned long portPrintf(const char *pcFormat, ...);
void vPutsToUART(char cChar);

#if defined __cplusplus
}
#endif

#endif /* PRINTF_H_INCLUDED */


