/*
 * SPIbackend.h
 *
 *  Created on: 08.12.2011
 *      Author: tolyan
 */

#ifndef SPIBACKEND_H_
#define SPIBACKEND_H_

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#ifdef __cplusplus
extern "C"
    {
#endif

#define SPI_MAX_TRANSACTION_TIME	(5)
#define SPI_NUM_OF_SLAVES		(2) // колличество аппаратно-управляемых слейв CS'ов
#define SPI_LSB_FIRST			(0) // орядок передачи битов начиная с младшего (0/1)
#define SPI_TX_CLOCK_AGE_1		(0) // передача по фронту/срезу SCK
#define SPI_RX_CLOCK_AGE_0		(0) // прием по срезу/фронту SCK
#define SPI_CLOCK_DEVIDER		(63) // делитель частоты SPI (0..63) Fsck = 16 Mhz / (8*SPI_CLOCK_DEVIDER)
#define SPI_USED_CS_MASK		(1<<1 | 1<<2)

    enum enSPI_ERRORS
	{
	SPI_ERR_OK,
	SPI_ERR_ALLREADY_STARTED, // этот поток уже захватил интерфейс
	SPI_ERR_OWNER = 0xff,
	// другой поток пытается чтото сделать с уже открытым интерфейсом
	};

    typedef struct
	{
	xSemaphoreHandle xSPIMutex;
	xTaskHandle xSPIOwner;
	unsigned char ucSC;
	} sSPIThreadControl;

    char ucSPITransfer8(char cByteToSend);
    enum enSPI_ERRORS enSPIStart(unsigned char ucCS, bool bLsbFirst, bool bTxNegEdge, bool bRxNegEdge, unsigned char u8ClockDivider);
    enum enSPI_ERRORS enSPIStop(void);
    void vSPIReset(void);

#ifdef __cplusplus
    }
#endif

#endif /* SPIBACKEND_H_ */
