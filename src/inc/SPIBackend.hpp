/*
 * SPIbackend.h
 *
 *  Created on: 08.12.2011
 *      Author: tolyan
 */

#ifndef SPIBACKEND_H_
#define SPIBACKEND_H_

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#define ADXL345_SS_PIN			(1 << 1)
#define L3G4200D_SS_PIN			(1 << 0)

#define SPICONFIG_LSB_FIRST_DEFAULT	(0)		// передавать младшим битом вперед?
#define SPICONFIG_TX_NEG_DEFAULT	(1)		// так работает с инвертором
#define SPICONFIG_RX_NEG_DEFAULT	(1)		// линии SCK
#define SPICONFIG_CLOCK_DIVIDER_DEFAULT	(32)		// делитель частоты F=16MHz/(2*SPICONFIG_CLOCK_DIVIDER_DEFAULT) 0..63
#define SPICONFIG_CS_MASK_DEFAULT	(ADXL345_SS_PIN | L3G4200D_SS_PIN)	// какие линии используются как SS


enum enhwSPI_ERRORS
    {
    SPI_ERR_OK,
    SPI_ERR_ALLREADY_STARTED, // этот поток уже захватил интерфейс
    SPI_ERR_STOP_THEWOUT_START, // запущено стоп без старта
    SPI_ERR_INVALID_CS,	      // этот пин не заявлен как используемый под SS
    SPI_ERR_OWNER = 0xff,
    // другой поток пытается чтото сделать с уже открытым интерфейсом
    };

class hwSPI
    {
public:
    hwSPI();
    void vReset(void);
    enum enhwSPI_ERRORS enSetDataEdges(bool bTxNegEdge, bool bRxNegEdge);
    enum enhwSPI_ERRORS enSetLsbFirst(bool bLsbFirst);
    enum enhwSPI_ERRORS enSetClockDivider(unsigned char ucClockDivider);

    enum enhwSPI_ERRORS enStart(unsigned long ulCS);
    enum enhwSPI_ERRORS enStop(void);

    unsigned char ucTransfer8(unsigned char cByteToSend);
    enum enhwSPI_ERRORS enTransferBuff(char* pucBuff,
	    unsigned long ulLength);

private:
    bool bLsbFirst;
    bool bTxNegEdge;
    bool bRxNegEdge;
    unsigned char ucClockDivider;

    xSemaphoreHandle xhwSPIMutex;
    xTaskHandle xhwSPIOwner;

    unsigned long ucCurrentCS;
    unsigned long ulUsedCSMask;
    };

#endif /* SPIBACKEND_H_ */
