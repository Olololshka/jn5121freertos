
#ifndef __SPI_H__
#define __SPI_H__

#ifdef __cplusplus
extern "C"
    {
#endif

/*
#define PORT_DIR(s, in_out) \
	(in_out ? vAHI_DioSetDirection() : vAHI_DioSetDirection())
*/
#define PORT_SET(s) \
	vAHI_DioSetOutput(1 << s.PortLine, 0)

#define PORT_CLR(s) \
	vAHI_DioSetOutput(0, 1 << s.PortLine)

#define PORT_PIN(s) \
	u32AHI_DioReadInput() & (1 << s.PortLine)


#define IO_MISO_DIR(in_out)	PORT_DIR(s->MISO, in_out)
#define IO_MOSI_DIR(in_out)	PORT_DIR(s->MOSI, in_out)
#define IO_SCK_DIR(in_out)	PORT_DIR(s->SCK, in_out)

#define IO_MISO_0			PORT_CLR(s->MISO)
#define IO_MOSI_0			PORT_CLR(s->MOSI)
#define IO_SCK_0			PORT_CLR(s->SCK)

#define IO_MISO_1			PORT_SET(s->MISO)
#define IO_MOSI_1			PORT_SET(s->MOSI)
#define IO_SCK_1			PORT_SET(s->SCK)

#define IO_MISO_READ		PORT_PIN(s->MISO)
#define IO_MOSI_READ		PORT_PIN(s->MOSI)
#define IO_SCK_READ			PORT_PIN(s->SCK)


/*
	D0 - SS
	D1 - MISO
	D2 - MOSI
	D3 - SCK
*/
#define SSPI_SS			(1<<0)
#define SSPI_MISO		(1<<1)
#define SSPI_M0SI		(1<<2)
#define SSPI_SCK		(1<<3)

typedef struct
	{
	void* AddressPortSet; //Адрес регистра устанавливающего на линии порта 1.
	void* AddressPortClr; //Адрес регистра устанавливающего на линии порта 0.
	void* AddressPortPin; //Адрес регистра возвращающего состояние порта.
	void* AddressPortDir; //Адрес регистра управляющего направлением передачи порта.
	char  PortLine; 	  //Номер линии порта.
	} port_str;

typedef struct
	{
	void* BaseAddress;	//ТОЛЬКО ДЛЯ АППАРАТНОЙ РЕАЛИЗАЦИИ!!!
	port_str MISO;		//Master In Slave Out.
	port_str MOSI;		//Master Out Slave In.
	port_str SCK;		//Линия синхронизации.
	char delay_us;		//Задержка между передачей битов в микросекундах.
	char _dord_ : 1;	//Порядок передачи бит:
						// 0 - первым будет передаваться старший бит;
						// 1 - первым будет передаваться младший бит.
	char _cpol_ : 1;	//Полярность тактового сигнала.
	char _cpha_ : 1;	//Фаза выборки.
	} spi_str;

void prgSPI_Init(spi_str* s);
char prgSPI_MasterExchange(spi_str* s, char tx_data);

#ifdef __cplusplus
    }
#endif

#endif /* __SPI_H__ */
