#ifndef _COMMON_H_
#define _COMMON_H_

#include <stdlib.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef FALSE
#define FALSE	(0)
#endif

#ifndef TRUE
#define TRUE	(1)
#endif

#define MY_LED_PIN			(16)

#define PRINTFMOD			(1)

// макросы упарвления флагами
#define _SET_FLAG(Flags, Flag)   	(Flags |= (Flag))
#define _UNSET_FLAG(Flags, Flag) 	(Flags &= ~(Flag))
#define _TOUGLE_FLAG(Flags, Flag) 	(Flags ^= (Flag))

//вызывает цункцию по указателю, если он не NULL
#define _EX_IF_EXISTS(fPointer, Args)	do { if (fPointer)\
						(fPointer)(Args); \
					    } \
					while (0)

#define _MUTEX_TAKE(Mutex)		do {\
					    if (Mutex)\
						while (xSemaphoreTake((Mutex), portMAX_DELAY) != pdPASS)\
							;\
					    }\
					while (0)

#define _MUTEX_GIVE(Mutex)		do {\
					    if (Mutex)\
						xSemaphoreGive(Mutex); \
					    } \
					while (0)

void *pvPortMalloc( size_t xWantedSize );
void vPortFree( void *pv );

#define _malloc				pvPortMalloc
#define _free				vPortFree

#define PORT_IN_STATE			(0)
#define PORT_OUT_STATE			(1)

#define max(a,b)			((a > b) ? (a) : (b))
#define min(a,b)			((a < b) ? (a) : (b))

void* pvMyRealloc(void* pvOldPlace, size_t xNewSize);
signed long slStr2LongAuto(char* pcString);
unsigned long ulStr2ULongAuto(char* pcString);

#ifdef __cplusplus
}
#endif

#endif
