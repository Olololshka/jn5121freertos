/*
 FreeRTOS.org V4.7.0 - Copyright (C) 2003-2007 Richard Barry.

 This file is part of the FreeRTOS.org distribution.

 FreeRTOS.org is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 FreeRTOS.org is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with FreeRTOS.org; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

 A special exception to the GPL can be applied should you wish to distribute
 a combined work that includes FreeRTOS.org, without being obliged to provide
 the source code for any proprietary components.  See the licensing section
 of http://www.FreeRTOS.org for full details of how and when the exception
 can be applied.

 ***************************************************************************
 See http://www.FreeRTOS.org for documentation, latest information, license
 and contact details.  Please ensure to read the configuration and relevant
 port sections of the online documentation.

 Also see http://www.SafeRTOS.com a version that has been certified for use
 in safety critical systems, plus commercial licensing, development and
 support options.
 ***************************************************************************
 */

/*
 * Creates all the demo application tasks, then starts the scheduler.  The WEB
 * documentation provides more details of the demo application tasks.
 *
 * This demo is configured to execute on a contoller or sensor board from a
 * Jennic Evaluation Kit.
 *
 * Main. c also creates a task called 'Check'.  This only executes every three
 * seconds but has the highest priority so is guaranteed to get processor time.
 * Its main function is to check that all the other tasks are still operational.
 * Each task that does not flash an LED maintains a unique count that is
 * incremented each time the task successfully completes its function.  Should
 * any error occur within such a task the count is permanently halted.  The
 * 'check' task inspects the count of each task to ensure it has changed since
 * the last time the check task executed.  If all the count variables have
 * changed all the tasks are still executing error free, and the check task
 * toggles an LED with a three second period.  Should any task contain an error
 * at any time the LED toggle rate will increase to 500ms.
 *
 */

/* Standard includes. */
#include <stdlib.h>
#include <signal.h>

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"

/* Platform Includes */
#include <AppApi.h>
#include <AppHardwareApi.h>
#include <gdb.h>

#include "LedDriver.h"
#include "common.h"
#include "SPIbackend.hpp"

extern void vAxelerometerReadTask(void);
extern void vGyroscopeReadTask(void);

extern hwSPI JN5121SPI;

#ifdef __cplusplus
extern "C"
    {
#endif

    extern void vLedTougleTask1(void* pvThreadArgument);
    extern void vLedTougleTask2(void* pvThreadArgument);
    extern void vPSTask(void);
    extern void vSPIFlashTalk(void);
    extern void vPrintSensorData(void);

    int main(void);

    typedef void (*pfConstructor)(void);

    /*
     * Perform the hardware setup required by the JN5139 in order to run the demo
     * application.
     */
    static void prvSetupHardware(void);

    /*-----------------------------------------------------------*/

    void AppColdStart(void)
	{
#ifdef GDB
	HAL_GDB_INIT();
	vAHI_UartSetClockDivisor(E_AHI_UART_0, E_AHI_UART_RATE_38400);
	HAL_BREAKPOINT();
#endif
	prvSetupHardware();
	}

    /*-----------------------------------------------------------*/

    void AppWarmStart(void)
	{
#ifdef GDB
	HAL_GDB_INIT();
	vAHI_UartSetClockDivisor(E_AHI_UART_0, E_AHI_UART_RATE_38400);
	HAL_BREAKPOINT();
#endif
	prvSetupHardware();
	}

    int main(void)
	{
	unsigned portBASE_TYPE shTLT1per = 500;
	unsigned portBASE_TYPE shTLT2per = 250;

	xTaskCreate((pdTASK_CODE)vLedTougleTask1, (const signed char*)"TLT1",
		configMINIMAL_STACK_SIZE, (void*)&shTLT1per, 3, NULL);

	xTaskCreate((pdTASK_CODE)vLedTougleTask2, (const signed char*)"TLT2",
		configMINIMAL_STACK_SIZE, (void*)&shTLT2per, 3, NULL);

	/*xTaskCreate((pdTASK_CODE)vPSTask, (const signed char*)"PSTASK", 256,
	 NULL, 2, NULL);
	 */
	/*
	 xTaskCreate((pdTASK_CODE)vSPIFlashTalk, (const signed char*)"SPIFLOOD",
	 configMINIMAL_STACK_SIZE + 100, NULL, 4, NULL);
	 */

	xTaskCreate((pdTASK_CODE)vPrintSensorData, (const signed char*)"SensD",
		configMINIMAL_STACK_SIZE, NULL, 2, NULL);

	xTaskCreate((pdTASK_CODE)vAxelerometerReadTask,
		(const signed char*)"Accel", configMINIMAL_STACK_SIZE, NULL, 4,
		NULL);

	xTaskCreate((pdTASK_CODE)vGyroscopeReadTask, (const signed char*)"Gyro",
		configMINIMAL_STACK_SIZE, NULL, 4, NULL);

	/* Start the scheduler. */
	vTaskStartScheduler();

	/* As the scheduler has been started the demo applications tasks will be
	 executing and we should never get here! */
	return 0;
	}
    /*-----------------------------------------------------------*/

    static void prvSetupHardware(void)
	{
	/* Initialise stack and hardware interfaces.  */
	//(void)u32AppApiInit(NULL,NULL,NULL,NULL,NULL,NULL);
	(void) u32AHI_Init(); //инициализация переферии
#ifdef __cplusplus
	extern void __libc_init_array(void);
	extern void __libc_fini_array(void);
	__libc_init_array();
	JN5121SPI.vReset();
#endif
	main();
#ifdef __cplusplus
	__libc_fini_array();
#endif
	while (1)
	    ;
	}

    /*-----------------------------------------------------------*/

#ifdef __cplusplus
    }
#endif
