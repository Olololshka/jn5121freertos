/*
 * mytasks.c
 *
 *  Created on: 20.10.2011
 *      Author: shiloxyz
 */

#include <stdlib.h>
#include <string.h>

#include <AppHardwareApi.h>

#include "FreeRTOS.h"
#include "task.h"

#include "common.h"
#include "CppIncs.hpp"
#include "Printf.h"
#include "LedDriver.h"
#include "SPIbackend.h"

#include "S_SPI.h"
#include "ADXL345.h"
#include "L3G4200D.h"

#ifdef __cplusplus
extern "C"
    {
#endif

    typedef union
	{
	unsigned long ulAddress;
	unsigned char mucData[4];
	} unAdress;

    void vLedTougleTask1(void* pvThreadArgument)
	{
	register portTickType xDelay = *(portBASE_TYPE*) pvThreadArgument;
	vLedInit(1 << 12);
	while (1)
	    {
	    vLedTougle(1 << 12);
	    vTaskDelay(xDelay);
	    }
	vTaskDelete(xTaskGetCurrentTaskHandle()); //самвыпилицо!
	}

    void vLedTougleTask2(void* pvThreadArgument)
	{
	register portTickType xDelay = *(portBASE_TYPE*) pvThreadArgument;
	vLedInit(1 << 16);
	while (1)
	    {
	    vLedTougle(1 << 16);
	    vTaskDelay(xDelay);
	    }
	vTaskDelete(xTaskGetCurrentTaskHandle()); //самвыпилицо!
	}

    //extern void vPutsToUART(char cChar);
    //extern void vUARTReset(unsigned char ucPort, unsigned char ucBoud);

    char pcData[] =
	{
	0xAB, // COMMAND_READ_ELECTRONIC_SIGNATURE
		0xFF, // DUMMY_BYTE
		0xFF, // DUMMY_BYTE
		0xFF, // DUMMY_BYTE
		0x00
	//
	    };

    char pcTbf[sizeof(pcData)];

    void vPSTask(void)
	{
	char tbf[5 * 40];
	//static char tbf[] = "Test String\r\n";
	unsigned long c = 0;
	char* t;
	while (1)
	    {
	    //	memset(tbf, 0, sizeof(tbf));
	    vTaskList((signed char*) tbf);
	    portPrintf("%s", tbf);
	    //	portPrintf("Try %u, %s, %s\n\r", c++,"ololol", "shkololo");
	    //	ulU_Printf(vPutsToUART ,"%d\t%s", c++,tbf);
	    vTaskDelay(50);
	    }
	vTaskDelete(xTaskGetCurrentTaskHandle()); //самвыпилицо!
	}

    extern spi_str sSpi;

    void vSPIFlashTalk(void)
	{
	unAdress ulAdress;
	ulAdress.ulAddress = 0;
	unsigned char t;
	unsigned char data[29];
	unsigned long i;
	prgSPI_Init(&sSpi);
	/*
	 vAHI_DioSetOutput(0, SSPI_SS);
	 prgSPI_MasterExchange(&sSpi, BIN8(00000000) | 0x31);
	 prgSPI_MasterExchange(&sSpi, BIN8(00001000));
	 vAHI_DioSetOutput(SSPI_SS, 0);
	 */
	/*
	 vAHI_DioSetOutput(0, 1 << 8);
	 prgSPI_MasterExchange(&sSpi, 0x20);
	 prgSPI_MasterExchange(&sSpi, BIN8(00001111));
	 vAHI_DioSetOutput(1 << 8, 0);
	 */
	while (1)
	    {
	    /*
	     enSPIStart(1<<1, 0, 1, 1, 63);
	     ucSPITransfer8(pcData[0]);
	     ucSPITransfer8(pcData[1]);
	     ucSPITransfer8(pcData[2]);
	     ucSPITransfer8(pcData[3]);
	     t = ucSPITransfer8(0xff);
	     enSPIStop();
	     portPrintf("Status: %x\n\r", t);
	     ulAdress.ulAddress++;
	     vTaskDelay(5);
	     */
#if 1
	    vAHI_DioSetOutput(0, 1 << 0);
	    prgSPI_MasterExchange(&sSpi, (1 << 7) | 0x00);
	    t = prgSPI_MasterExchange(&sSpi, 0xFF);
	    vAHI_DioSetOutput(1 << 0, 0);

	    vAHI_DioSetOutput(0, 1 << 0);
	    prgSPI_MasterExchange(&sSpi, (1 << 7) | (1 << 6) | 0x1d);
	    for (i = 0; i < sizeof(data); ++i)
		data[i] = prgSPI_MasterExchange(&sSpi, 0xFF);
	    vAHI_DioSetOutput(1 << 0, 0);

	    portPrintf("Accelerometer ID: %x\n\r", t);
	    portPrintf("Data:\n\r");
	    for (i = 0x1d; i < (sizeof(data) + 0x1d); ++i)
		portPrintf("\t%x: %x\n\r", i, data[i]);
	    portPrintf("\n\r");
#else
	    enSPIStart(1 << 1, 0, 0, 1, 20);
	    ucSPITransfer8((1 << 7) | 0x00);
	    t = ucSPITransfer8(0xff);
	    enSPIStop();
	    portPrintf("Accelerometer ID: %x\n\r", t);
#endif
	    /*
	     enSPIStart(1<<2, 0, 1, 1, 10);
	     ucSPITransfer8(1<<7 | 0x0F); //Gyroscope ID
	     t = ucSPITransfer8(0x00);
	     enSPIStop();
	     portPrintf("Gyroscope ID: %x\n\r", t);
	     */
	    /*
	     vAHI_DioSetOutput(0, 1 << 8);
	     prgSPI_MasterExchange(&sSpi, (1 << 7) | 0x0F);
	     t = prgSPI_MasterExchange(&sSpi, 0xFF);
	     vAHI_DioSetOutput(1 << 8, 0);
	     portPrintf("Gyroscope ID: %x\n\r", t);
	     */
	    vTaskDelay(50);
	    }
	vTaskDelete(xTaskGetCurrentTaskHandle()); //самвыпилицо!
	}

    int iAccelerationsXYZ[3];
    int iAngleAccelerationOmegaXOmegaYOmegaZ[3];

    void vPrintSensorData(void)
	{
	while (1)
	    {
	    portPrintf("a:\t\tX: %i\tY: %i\tZ: %i\n\r", iAccelerationsXYZ[0],
		    iAccelerationsXYZ[1], iAccelerationsXYZ[2]);
	    portPrintf("Omega_a:\tX: %i\tY: %i\tZ: %i\n\r",
		    iAngleAccelerationOmegaXOmegaYOmegaZ[0],
		    iAngleAccelerationOmegaXOmegaYOmegaZ[1],
		    iAngleAccelerationOmegaXOmegaYOmegaZ[2]);
	    vTaskDelay(100);
	    }
	vTaskDelete(xTaskGetCurrentTaskHandle()); //самвыпилицо!
	}

#ifdef __cplusplus
    }
#endif

/*
 * Плюсовый задачи
 */

void vAxelerometerReadTask(void)
    {
    ADXL345 Accel;
    Accel.set_bw(ADXL345_BW_6);

    Accel.powerOn();
    while (1)
	{
	Accel.readAccel(iAccelerationsXYZ);
	vTaskDelay(10);
	}
    vTaskDelete(xTaskGetCurrentTaskHandle()); //самвыпилицо!
    }

void vGyroscopeReadTask(void)
    {
    L3G4200D Gyro;
    Gyro.enableDefault();
    while (1)
	{
	Gyro.read();
	Gyro.vector_normalize(&Gyro.g);
	iAngleAccelerationOmegaXOmegaYOmegaZ[0] = (int)(Gyro.g.x * 1000);
	iAngleAccelerationOmegaXOmegaYOmegaZ[1] = (int)(Gyro.g.y * 1000);
	iAngleAccelerationOmegaXOmegaYOmegaZ[2] = (int)(Gyro.g.z * 1000);
	vTaskDelay(10);
	}
    vTaskDelete(xTaskGetCurrentTaskHandle()); //самвыпилицо!
    }
